package com.api.reversegear.dto.user;

import lombok.Data;

@Data
public class ResetPassword {

    private String password;
    private String emailAddress;
    private String otp;
}
