package com.api.reversegear.dto.user;

import lombok.Data;

@Data
public class UserActivationDTO {
    private String newPassword;
    private String oldPassword;
}
