package com.api.reversegear.dto.user;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class Authentication {

    @NotNull
    @Size(min = 1, max = 100)
    private String emailId;

    @NotNull
    private String password;
}
