package com.api.reversegear.dto.supplier;

import lombok.Data;

@Data
public class SupplierDTO {
    private String supplierId;
    private String firstName;
    private String lastName;
    private String contactNumber;
    private String emailAddress;
}
