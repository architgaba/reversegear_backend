package com.api.reversegear.repository.supplier;

import com.api.reversegear.domain.supplier.Supplier;
import com.api.reversegear.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier, Long> {
    Supplier findByUser(User user);
}
