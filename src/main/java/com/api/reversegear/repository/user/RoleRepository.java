package com.api.reversegear.repository.user;

import com.api.reversegear.domain.user.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<UserRole, Long> {

    UserRole findByRoleCode(Integer roleCode);
}
