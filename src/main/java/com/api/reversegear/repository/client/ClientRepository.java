package com.api.reversegear.repository.client;

import com.api.reversegear.domain.client.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    Client findByEmailAddress(String emailAddress);
}
