package com.api.reversegear.service.user;

import com.api.reversegear.domain.user.User;
import com.api.reversegear.dto.user.Authentication;
import com.api.reversegear.dto.user.ResetPassword;
import com.api.reversegear.dto.user.UserActivationDTO;
import org.springframework.http.ResponseEntity;

public interface UserService {
    ResponseEntity<?> userAuthentication(Authentication authentication);

    ResponseEntity<?> userRegistration(User user,String url);

    ResponseEntity<?> forgotPassword(String emailId);

    ResponseEntity<?> resetPassword(ResetPassword resetPassword);

    ResponseEntity<?> customerAccountActivate(String activationKey, String password);

    boolean redirectView(String id);

    ResponseEntity<?> authorize(String authKey);

    ResponseEntity<?> getUserInformation(String auth);

    ResponseEntity<?> updatePassword(String auth, UserActivationDTO userActivationDTO);

    ResponseEntity<?> updateProfile(String auth, User user);
}
