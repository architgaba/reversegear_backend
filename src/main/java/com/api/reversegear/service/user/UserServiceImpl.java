package com.api.reversegear.service.user;

import com.api.reversegear.domain.client.Client;
import com.api.reversegear.domain.user.User;
import com.api.reversegear.dto.user.Authentication;
import com.api.reversegear.dto.user.ResetPassword;
import com.api.reversegear.dto.user.UserActivationDTO;
import com.api.reversegear.repository.client.ClientRepository;
import com.api.reversegear.repository.user.RoleRepository;
import com.api.reversegear.repository.user.UserRepository;
import com.api.reversegear.repository.user.UserStatusRepository;
import com.api.reversegear.security.TokenProvider;
import com.api.reversegear.utils.ResponseDomain;
import com.api.reversegear.utils.converter.CustomDateConverter;
import com.api.reversegear.utils.mail.EmailServiceImpl;
import com.api.reversegear.utils.mail.MailModel;
import com.api.reversegear.utils.response.AppProperties;
import com.api.reversegear.utils.response.MessageProperties;
import io.jsonwebtoken.Claims;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TokenProvider tokenProvider;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AppProperties appProperties;
    @Autowired
    private EmailServiceImpl emailService;
    @Autowired
    private MessageProperties messageProperties;
    @Autowired
    private UserStatusRepository userStatusRepository;
    @Autowired
    private ClientRepository clientRepository;

    private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

    @Override
    public ResponseEntity<?> userAuthentication(Authentication authentication) {
        logger.info("method ::: userAuthentication");
        try {
            User user = userRepository.findByEmailAddress(authentication.getEmailId());
            if (user == null)
                return ResponseDomain.badRequest("No Such User Exists with the mentioned username");
            if (user.getAccountActivationKey() != null || user.getPassword() == null)
                return ResponseDomain.badRequest(messageProperties.getAccountVerification());
            JSONObject object = new JSONObject();
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user.getUserId(), authentication.getPassword());
            org.springframework.security.core.Authentication authentication1 = this.authenticationManager.authenticate(authenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authentication1);
            String jwt = tokenProvider.createToken(authentication1, authentication.getEmailId(), user.getUserId());
            object.put("token", jwt);
            user.setLastSignedInDate(LocalDateTime.now());
            userRepository.save(user);
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add(TokenProvider.AUTHORIZATION_HEADER, "Bearer " + jwt);
            return new ResponseEntity<>(object, httpHeaders, HttpStatus.OK);
        } catch (BadCredentialsException e) {
            return ResponseDomain.badRequest("Invalid username or password.");
        } catch (Exception e) {
            return ResponseDomain.internalServerError(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<?> userRegistration(User user, String url) {
        logger.info("method ::: userRegistration");
        user.setRole(roleRepository.findByRoleCode(3));
        User userTest = userRepository.findByEmailAddress(user.getEmailAddress());
        if (userTest != null) {
            return ResponseDomain.badRequest("User already exists with mentioned email address , Please Login");
        } else {
            user.setUserStatus(userStatusRepository.findById(3l).get());
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user = userRepository.save(user);
            String jwt = tokenProvider.accountActivationToken(user);
            user.setAccountActivationKey(jwt);
            user = userRepository.save(user);
            url = url + "/user/activate/account/" + jwt;
            emailService.userAccountVerification(url, "registration", user);
            return ResponseDomain.postResponse("" + messageProperties.getRegisteredSuccessfully());
        }
    }

    @Override
    public ResponseEntity<?> forgotPassword(String emailId) {
        logger.info("method ::: forgotPassword");
        User user = userRepository.findByEmailAddress(emailId);
        if (user != null) {
            Integer otp = generateRandomIntIntRange(appProperties.getMinSecretLength(), appProperties.getMaxSecretLength());
            user.setPasswordResetKey(tokenProvider.resetPasswordKey(user, otp));
            userRepository.save(user);
            emailService.sendPlainMessage(new MailModel(user.getEmailAddress(), appProperties.getSenderMail(), "OTP", otp.toString()));
            return ResponseDomain.successResponse("OTP Sent Successfully to the mentioned mail for resetting password");
        }
        return ResponseDomain.badRequest("User does not Exist");
    }

    public static int generateRandomIntIntRange(int min, int max) {
        logger.info("method ::: generateRandomIntIntRange");
        double x = (Math.random() * ((max - min) + 1)) + min;
        if (x > 999999)
            x = x / 10;
        return (int) x;
    }

    @Override
    public ResponseEntity<?> resetPassword(ResetPassword resetPassword) {
        logger.info("method ::: resetPassword");
        User user = userRepository.findByEmailAddress(resetPassword.getEmailAddress());
        if (user != null) {
            String status = tokenProvider.validateResetKey(user, resetPassword.getOtp());
            if (status.equalsIgnoreCase("success")) {
                user.setPassword(passwordEncoder.encode(resetPassword.getPassword()));
                user.setPasswordResetKey(null);
                user.setLastPasswordChangeDate(LocalDateTime.now());
                userRepository.save(user);
                emailService.sendPlainMessage(new MailModel(user.getEmailAddress(), appProperties.getSenderMail(), "Password Reset Successfully!!!", "Password Reset Successfully!!!"));
                return ResponseDomain.successResponse("Password Reset Successfully !!!");
            }
            return ResponseDomain.badRequest("Invalid OTP");
        }
        return ResponseDomain.badRequest("User does not Exist");
    }

    @Override
    public ResponseEntity<?> customerAccountActivate(String activationKey, String password) {
        logger.info("method ::: customerAccountActivate");
        Optional<User> user = userRepository.findById(Long.parseLong(tokenProvider.getUserIdFromAccountActivationToken(activationKey)));
        if (user.isPresent()) {
            Claims claimsFromToken = tokenProvider.getAllClaimsFromToken(activationKey);
            String role = claimsFromToken.get("role").toString();
            user.get().setAccountActivationKey(null);
            user.get().setUserStatus(userStatusRepository.findById(1l).get());
            user.get().setPassword(passwordEncoder.encode(password));
            userRepository.save(user.get());
            switch (role) {
                case "ROLE_CLIENT":
                    Client newClient = new Client();
                    clientRepository.save(newClient);
                    break;

                case "ROLE_CUSTOMER":
            }
            emailService.sendAccountActivationMailToUser(user.get());
            return ResponseDomain.putResponse("You are Successfully registered with Reverse Gear");
        }
        return ResponseDomain.badRequest("Your account currently not activated, Please activate it first");
    }

    @Override
    public boolean redirectView(String id) {
        logger.info("method ::: redirectView");
        return tokenProvider.validateAccountActivationToken(id);
    }

    @Override
    public ResponseEntity<?> authorize(String authKey) {
        logger.info("method ::: authorize");
        User currentUser;
        List<String> role = new ArrayList<>();
        try {
            currentUser = userRepository.findById(Long.parseLong(tokenProvider.getUserId(authKey))).get();
            role.add(currentUser.getRole().getRoleDescription());
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseDomain.badRequest(e.getMessage());
        }
        JSONObject res = new JSONObject();
        res.put("permission", currentUser.getUserStatus().getStatusDescription());
        res.put("name", currentUser.getFirstName() + " " + (currentUser.getLastName() == null ? "" : currentUser.getLastName()));
        res.put("role", role);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> getUserInformation(String auth) {
        logger.info("method ::: getUserInformation");
        User user = userRepository.findById(Long.parseLong(tokenProvider.getUserId(auth))).get();
        JSONObject res = new JSONObject();
        if (user != null) {
            res.put("userRegistrationId", user.getUserId());
            res.put("firstName", user.getFirstName());
            res.put("lastName", user.getLastName());
            res.put("emailAddress", user.getEmailAddress());
            res.put("creationDate", CustomDateConverter.dateToTimestamp(user.getCreationDate()));
            res.put("lastModifiedDate", CustomDateConverter.dateToTimestamp(user.getLastModifiedDate()));
            res.put("userStatus", user.getUserStatus().getStatusDescription());
            res.put("contactNumber", user.getContactNumber());
            res.put("lastPasswordChangeDate", user.getLastPasswordChangeDate() != null ? CustomDateConverter.localDateTimeToString(user.getLastPasswordChangeDate()) : "");
            res.put("lastSignedInDate", user.getLastSignedInDate() != null ? CustomDateConverter.localDateTimeToString(user.getLastSignedInDate()) : "");
            return new ResponseEntity<>(res, HttpStatus.OK);
        } else {
            return ResponseDomain.responseNotFound(messageProperties.getNotExist());
        }
    }

    @Override
    public ResponseEntity<?> updatePassword(String auth, UserActivationDTO userActivationDTO) {
        logger.info("method ::: updatePassword");
        Optional<User> user = userRepository.findById(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user.isPresent()) {
            if (passwordEncoder.matches(userActivationDTO.getOldPassword(), user.get().getPassword())) {
                user.get().setPassword(passwordEncoder.encode(userActivationDTO.getNewPassword()));
                user.get().setLastPasswordChangeDate(LocalDateTime.now());
                userRepository.save(user.get());
                return ResponseDomain.putResponse("Password Changed Successfully");
            }
            return ResponseDomain.badRequest("Incorrect Old Password");

        }
        return ResponseDomain.badRequest("User Does Not Exist");
    }

    @Override
    public ResponseEntity<?> updateProfile(String auth, User user) {
        logger.info("method ::: updateProfile");
        Optional<User> dbUser = userRepository.findById(Long.parseLong(tokenProvider.getUserId(auth)));
        if (dbUser.isPresent()) {
            dbUser.get().setFirstName(user.getFirstName());
            dbUser.get().setLastName(user.getLastName());
            dbUser.get().setContactNumber(user.getContactNumber());
            userRepository.save(dbUser.get());
            return ResponseDomain.putResponse("Profile Updated Successfully");
        }
        return ResponseDomain.badRequest("User Does Not Exist");
    }
}

