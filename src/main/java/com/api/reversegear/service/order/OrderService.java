package com.api.reversegear.service.order;

import com.api.reversegear.domain.order.Order;
import org.springframework.http.ResponseEntity;

public interface OrderService {

    ResponseEntity<?> createOrder(Order order, String authKey);

    ResponseEntity<?> getAllOrders();
    ResponseEntity<?> updateOrder(Order order,String authKey);
    ResponseEntity<?> deleteOrder(Order order,String authKey);

}


