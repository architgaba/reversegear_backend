package com.api.reversegear.service.order;

import com.api.reversegear.domain.client.Client;
import com.api.reversegear.domain.order.Order;
import com.api.reversegear.domain.user.User;
import com.api.reversegear.repository.client.ClientRepository;
import com.api.reversegear.repository.order.OrderRepository;
import com.api.reversegear.repository.user.UserRepository;
import com.api.reversegear.security.TokenProvider;

import com.sun.deploy.util.SessionState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class OrderServiceImpl implements OrderService {
    private static final Logger logger = LogManager.getLogger(OrderServiceImpl.class);

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TokenProvider tokenProvider;


    //create order
    @Override
    public ResponseEntity<?> createOrder(Order order, String authKey) {

        logger.info("method ::: createOrder");

        Client client=new Client();
        client.getClientId();

        order.setClient(client);

        orderRepository.save(order);

        return new ResponseEntity<>(order, HttpStatus.OK);
    }

  //read order
    @Override
    public ResponseEntity<?> getAllOrders() {
        logger.info("method ::: getAllSuppliers");
        List<Order> orders = orderRepository.findAll();
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    // UPDATE order
    @Override
    public ResponseEntity<?> updateOrder(Order order, String url) {
        logger.info("method ::: updateOrder");
        orderRepository.saveAndFlush(order);

        return new ResponseEntity<>(order, HttpStatus.OK);

    }


     //DELETE order
    @Override
    public ResponseEntity<?> deleteOrder(Order order, String url) {
        logger.info("method ::: DeleteOrder");

        orderRepository.delete(order);
        return new ResponseEntity<>(order, HttpStatus.OK);

    }
}










