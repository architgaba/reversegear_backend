package com.api.reversegear.service.supplier;

import com.api.reversegear.domain.user.User;
import org.springframework.http.ResponseEntity;

public interface SupplierService {


    ResponseEntity<?> createSupplier(User user, String url);

    ResponseEntity<?> getAllSuppliers();
}
