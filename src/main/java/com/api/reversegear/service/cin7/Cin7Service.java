package com.api.reversegear.service.cin7;

import org.springframework.http.ResponseEntity;

public interface Cin7Service {

    ResponseEntity<?> contacts(String filter);
}
