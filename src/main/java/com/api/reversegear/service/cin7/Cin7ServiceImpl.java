package com.api.reversegear.service.cin7;

import com.api.reversegear.utils.ResponseDomain;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class Cin7ServiceImpl implements Cin7Service {

    @Override
    public ResponseEntity<?> contacts(String filter) {
        String apiUrl = "https://api.cin7.com/api/v1/Contacts";
        apiUrl=apiUrl+(filter!=null?"?"+filter:"");
        System.out.println(apiUrl);
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        String plainCreds = "EcomGlobalIncUK:36de8b74ddde4ec19ffa858b766b6e9c";
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);
        headers.add("Authorization", "Basic " + base64Creds);
        HttpEntity<?> entity = new HttpEntity<>(headers);
        ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.GET, entity, String.class);
        if(response.getStatusCode().value()==200){
            return new ResponseEntity<>(response.getBody(), HttpStatus.OK);
        }else
            return ResponseDomain.badRequest();
    }
}


