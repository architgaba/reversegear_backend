package com.api.reversegear.service.client;

import com.api.reversegear.domain.client.Client;
import org.springframework.http.ResponseEntity;

public interface ClientService {
    ResponseEntity<?> createClient(Client client, String url);

    ResponseEntity<?> getAllSuppliers();
}
