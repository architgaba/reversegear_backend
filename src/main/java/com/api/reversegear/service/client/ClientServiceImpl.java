package com.api.reversegear.service.client;

import com.api.reversegear.domain.client.Client;
import com.api.reversegear.dto.supplier.SupplierDTO;
import com.api.reversegear.repository.client.ClientRepository;
import com.api.reversegear.repository.user.RoleRepository;
import com.api.reversegear.repository.user.UserStatusRepository;
import com.api.reversegear.security.TokenProvider;
import com.api.reversegear.utils.ResponseDomain;
import com.api.reversegear.utils.mail.EmailServiceImpl;
import com.api.reversegear.utils.response.MessageProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    private static final Logger logger = LogManager.getLogger(ClientServiceImpl.class);

    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private MessageProperties messageProperties;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private TokenProvider tokenProvider;
    @Autowired
    private EmailServiceImpl emailService;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserStatusRepository userStatusRepository;

    @Override
    public ResponseEntity<?> createClient(Client client, String url) {
        logger.info("method ::: createSupplier");
        Client clientObj = clientRepository.findByEmailAddress(client.getEmailAddress());
        if (clientObj != null) {
            return ResponseDomain.badRequest("User already exists with mentioned email address , Please Login");
        } else {
            client = clientRepository.save(client);
//            String jwt = tokenProvider.accountActivationToken(client);
//            client.setAccountActivationKey(jwt);
//            user = userRepository.save(user);
//            url = url + "/user/activate/" + jwt;
//            emailService.userAccountVerification(url, "registration", user);
            return ResponseDomain.postResponse("" + messageProperties.getRegisteredSuccessfully());
        }
    }

    @Override
    public ResponseEntity<?> getAllSuppliers() {
        logger.info("method ::: getAllSuppliers");
        List<SupplierDTO> supplierList = new ArrayList<>();
        List<Client> clientList = clientRepository.findAll();

        return new ResponseEntity<>(clientList, HttpStatus.OK);
    }
}
