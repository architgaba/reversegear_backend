package com.api.reversegear.utils.response;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties
@Data
public class AppProperties {

    private Integer minSecretLength;
    private Integer maxSecretLength;
    private String senderMail;

    private String loginPageRedirect;
    private String loginPageRedirectError;
    private String userRedirect;
}
