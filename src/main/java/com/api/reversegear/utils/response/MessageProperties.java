package com.api.reversegear.utils.response;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:message.properties")
@ConfigurationProperties(prefix = "user")
@Data
public class MessageProperties {

    private String resetPasswordLinkGenerate;
    private String resetPasswordLinkExpired;
    private String resetPasswordInvalidOTP;
    private String resetPasswordSuccess;
    private String registeredSuccessfully;
    private String accountVerification;
    private String notExist;
    private String invalidInput;
    private String usernameAlreadyExist;
    private String accountNotVerified;
    private String usernameNotAvailable;

}
