package com.api.reversegear.utils.mail;

import com.api.reversegear.domain.user.User;
import com.api.reversegear.utils.response.AppProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class EmailServiceImpl {

    @Autowired
    public JavaMailSender emailSender;
    @Autowired
    private AppProperties appProperties;

    public void sendPlainMessage(MailModel mailModel) {
        MimeMessage mail = emailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(mail, true);
            helper.setTo(mailModel.getTo());
            helper.setFrom(mailModel.getFrom());
            helper.setSubject(mailModel.getSubject());
            helper.setText(mailModel.getMessage(), true);
            emailSender.send(mail);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public void userAccountVerification(String url, String operationType, User user) {
        MimeMessage mail = emailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(user.getEmailAddress());
            helper.setFrom(appProperties.getSenderMail());
            switch (operationType) {
                case "registration":
                    helper.setSubject("Welcome To The Reverse Gear");
                    helper.setText(EmailContent.registrationEmailContent(user.getFirstName(), url), true);
                    emailSender.send(mail);
                    break;
                default:
                    break;
            }
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public boolean sendAccountActivationMailToUser(User user) {
        MimeMessage mail = emailSender.createMimeMessage();
        boolean isMailSend = false;
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(user.getEmailAddress());
            helper.setFrom(appProperties.getSenderMail());
            helper.setSubject("ReverseGear Account Activation");
            String name = user.getFirstName() + (user.getLastName() != null ? (" " + user.getLastName()) : "");
            helper.setText(EmailContent.accountActivationEmailContent(name, user.getFirstName()), true);
            emailSender.send(mail);
            isMailSend = true;
        } catch (MessagingException e) {
            isMailSend = false;
        }
        return isMailSend;
    }
}
