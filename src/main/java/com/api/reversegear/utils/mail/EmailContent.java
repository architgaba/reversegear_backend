package com.api.reversegear.utils.mail;

import org.springframework.stereotype.Component;

/**
 * Email Content Util
 * For Account Activation &
 * Password Reset
 */
@Component
public class EmailContent {


    /**
     * Mail Content For New
     * User Registration
     *
     * @param userName
     * @param url
     * @return registration email content
     */
    public static String registrationEmailContent(String userName, String url) {

        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +

                "<h2 style=\"  font-size: 25px;font-weight: 700;color: #333;text-align: center;margin-bottom: 1rem;\">Welcome To The Reverse Gear </h2>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Congratulations on becoming a Reverse Gear User. Listed below is the key element of the" +
                "transaction you have recently completed with us. Please be sure to save this email or make a note of your unique username.</p>" +

                "<p style=\"font-size: 14px;font-weight: 500; background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Your User Name for future visits is: " +
                "<span style=\"font-size: 14px;font-weight: 500;padding: 2px;\">" + userName + "</span></p>" +
                "<p style=\" font-size: 14px;font-weight: 500;background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Please click on the below button to activate your account :</p>" +


                "<div  style=\"margin-top: 20px;margin-bottom: 20px;text-align: left;\">" +
                "<a style=\"background: #fff;padding: 8px 60px;border-radius: 5px;color: #328338;font-size: 13px; font-weight: 700;letter-spacing: 1px; text-decoration: none;\" href=\"" + url + "\" >" + "Activate" + "</a>" +
                "</div>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">To set up your personal Reverse Gear Account, go directly to the *App Store* for IPhone users " +
                "and *Play Store* for Android users.  Download the app(Reverse Gear), once it's downloaded log in using the same " +
                "username and password you used when you created your Reverse Gear Account.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">If you have any issues or need any assistance, contact customer support at " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@reversegear.com</a>.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Yours in Success,</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Your Dedicated Support Team at Reverse Gear</p>" +
                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from Reverse Gear, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }


    /**
     * Mail Content For
     * Reset Password OTP
     *
     * @param firstName
     * @param lastName
     * @param userName
     * @param otp
     * @return email content fo OTP
     */
    public static String resetPasswordEmailContent(String firstName, String lastName, String userName, String otp) {

        lastName = lastName != null ? lastName : "";
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Hi, " + firstName + " " + lastName + " ( " + userName + " )</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Please, use the OTP Code listed below to reset your password</p>" +
                "<p style=\" background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">OTP(One Time Password) : " +
                "<span style=\"padding: 2px;\">" + otp + "</span></p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Please do not reply to this message; Send any questions" +
                "regarding your Money Flow Manager Account " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@reversegear.com</a>.</p>" +

                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }

    /**
     * Email Content For
     * Success Password
     *
     * @param firstName
     * @param lastName
     * @param userName
     * @param passwordUpdateDate
     * @return Success Password Mail Content
     */
    public static String successPasswordEmailContent(String firstName, String lastName, String userName, String passwordUpdateDate) {
        lastName = lastName != null ? lastName : "";

        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Hi, " + firstName + " " + lastName + " ( " + userName + " )</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Your Password has been reset successfully on " + passwordUpdateDate + "</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Please do not reply to this message; Send any questions" +
                "regarding your Money Flow Manager Account " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@moneyflowapp.com</a>.</p>" +

                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }


    public static String customerRegistrationEmailContent(String url, String referrerCode) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +

                "<h2 style=\"  font-size: 25px;font-weight: 700;color: #333;text-align: center;margin-bottom: 1rem;\">Welcome To P2P Connection, LLC </h2>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Listed below is the activation button to be used to set up your account as Customer." +
                "Once you click activate, follow the instructions to set up your personal account.</p>" +

                "<p style=\" background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Please click on the below button to activate your Personal Account.</p>" +
                "<p style=\" background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Referrer Code  " +
                "<span style=\"background-color:#add2b1;padding: 2px;\">" + referrerCode + "</span></p>" +

                "<div  style=\"margin-top: 20px;margin-bottom: 20px;    width: 9em;\">" +
                "<a href=\"" + url + "\">" +
                "<p style=\"background: #fff;padding: 8px 25px;border-radius: 5px;color: #328338;font-size: 13px; font-weight: 700;letter-spacing: 1px; text-decoration: none;\">" + "Activate" + "</p>" +
                "</a>" +
                "</div>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">To set up your personal Money Flow Manager Account, go directly to the *App Store* for IPhone users " +
                "and *Play Store* for Android users.  Download the app, once it's downloaded log in using the same " +
                "username and password you used when you created your Money Flow Manager Account.</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">If you have any issues or need any assistance, contact customer support at " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@moneyflowapp.com</a>.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Yours in Success,</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Your Dedicated Support Team at P2P Connection</p>" +
                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }

    public static String customerRepRegistrationEmailContent(String url, String referrerCode) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +

                "<h2 style=\"  font-size: 25px;font-weight: 700;color: #333;text-align: center;margin-bottom: 1rem;\">Welcome To P2P Connection, LLC </h2>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Listed below is the activation button to be used to set up your account as Customer Representative." +
                "Once you click activate, follow the instructions to set up your personal account.</p>" +

                "<p style=\" background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Please click on the below button to activate your Personal Account.</p>" +
                "<p style=\" background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Referrer Code  " +
                "<span style=\"background-color:#add2b1;padding: 2px;\">" + referrerCode + "</span></p>" +

                "<div  style=\"margin-top: 20px;margin-bottom: 20px;    width: 9em;\">" +
                "<a href=\"" + url + "\">" +
                "<p style=\"background: #fff;padding: 8px 25px;border-radius: 5px;color: #328338;font-size: 13px; font-weight: 700;letter-spacing: 1px; text-decoration: none;\">" + "Activate" + "</p>" +
                "</a>" +
                "</div>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">To set up your personal Money Flow Manager Account, go directly to the *App Store* for IPhone users " +
                "and *Play Store* for Android users.  Download the app, once it's downloaded log in using the same " +
                "username and password you used when you created your Money Flow Manager Account.</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">If you have any issues or need any assistance, contact customer support at " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@moneyflowapp.com</a>.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Yours in Success,</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Your Dedicated Support Team at P2P Connection</p>" +
                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }


    public static String accountActivationEmailContent(String name, String userName) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Hi, " + name + " " + "" + " ( " + userName + " )</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Your account is now active.</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Please do not reply to this message; Send any questions " +
                "regarding your Reverse Gear account  to " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@moneyflowapp.com</a> or call us at 800-674-2915.</p>" +

                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further Notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }

    public static String adminAccountActivationEmailContent(String adminName, String password) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Hi, " + adminName + "</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >your Account has been create with following credentials </p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >user name:" + adminName + " <br> password:" + password + "</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">The P2P Connection, LLC Team</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Please do not reply to this message; send any questions" +
                "regarding your P2P Connection, LLC moneyflowmanager to " +
                "<a style=\"color:#44a0b3; text-decoration:none;\" href=\"javascript:void(0);\">support@p2pconnection.net</a> or call us at 800-674-2915.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">This message is a service email related to your use of P2P Connection, LLC moneyflowmanager." +
                "For general inquiries or to request support with your P2P Connection, LLC moneyflowmanager, please visit us at " +
                "<a style=\"color:#44a0b3; text-decoration:none;\" href=\"http://www.p2pconnection.net\">http://www.p2pconnection.net</a> or call us at 800-674-2915 or send an email to " +
                "<a style=\"color:#44a0b3; text-decoration:none;\" href=\"support@p2pconnection.net\">support@p2pconnection.net</a></p>" +

                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">unsubscribe from receiving any further Notifications from P2P Connection, LLC - " +
                "<a style=\"color:#44a0b3; text-decoration:none;\" href=\"\"> click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }

    public static String subscriptionCancellation(String name) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 2em;\" >Hello " + name + "," +
                "We have received a notification that you have requested to cancel your access to the Money Flow Manager, you will not receive any further billing to your account. " +
                "Please contact us if you become interested in reinstating your account in the future. </p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">If you have any issues or need any assistance, contact customer support at " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@moneyflowapp.com</a>.</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Yours in Success,</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Your Dedicated Support Team at P2P Connection</p>" +
                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "</body>" +
                "</html>";
    }

    public static String newSubscription(String name) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 2em;\" >Hello " + name + "," +
                "Welcome to the P2P Money Flow Manager.  \n" +
                "Your next step is to go to the App Store if you have an Iphone or Play Store if you are using an Android phone, search for “P2P Money Flow Money Flow and download it on your phone.  " +
                "once it's downloaded log in using your same username and password you used when you created this account\n. </p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">If you have any issues or need any assistance, contact customer support at " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@moneyflowapp.com</a>.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Yours in Success,</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Your Dedicated Support Team at P2P Connection</p>" +
                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +
                "</body>" +
                "</html>";
    }

    public static String subscriptionPaymentFailed(String name, String cardNumber) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 2em;\" >Hello " + name + "," +
                "We were unable to charge your Visa ending in"+ cardNumber +"for your P2P Connection LLC subscription." +
                "Please login into the money flow manager application and update your billing information.</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">If you have any issues or need any assistance, contact customer support at " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@moneyflowapp.com</a>.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Yours in Success,</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Your Dedicated Support Team at P2P Connection</p>" +
                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +
                "</body>" +
                "</html>";
    }
}

