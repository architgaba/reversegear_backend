package com.api.reversegear.security;

import com.api.reversegear.domain.user.User;
import com.api.reversegear.repository.user.UserRepository;
import com.api.reversegear.repository.user.UserStatusRepository;
import com.api.reversegear.utils.response.MessageProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class TokenProvider {

    @Autowired
    private UserDetailService userDetailService;
    @Autowired
    private MessageProperties messageProperties;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserStatusRepository userStatusRepository;

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    private static final String AUTHORITIES_KEY = "auth";
    private String privateKey = "test";
    private long tokenValidityInMilliseconds = 172800000;
    private long accountActivationTokenValidityInMilliseconds = 172800000;
    private long resetKeyValidity = 600000;


    /**
     * Authorization Token Provider
     *
     * @param authentication
     * @param emailId
     * @return JWT Token
     */
    public String createToken(Authentication authentication, String emailId, Long id) {

        String authorities = authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));
        long now = (new Date()).getTime();
        Date validity = new Date(now + this.tokenValidityInMilliseconds);
        return Jwts.builder()
                .setId(id.toString())
                .setSubject(emailId)
                .setIssuer("ReverseGear")
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .claim(AUTHORITIES_KEY, authorities)
                .signWith(SignatureAlgorithm.HS256, privateKey)
                .setExpiration(validity)
                .compact();
    }


    /**
     * @param request
     * @return token
     */
    public String resolveToken(HttpServletRequest request) {
        String bearerToken = request.getHeader(AUTHORIZATION_HEADER);
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(TOKEN_PREFIX)) {
            return bearerToken.substring(7);
        }
        return null;
    }

    /**
     * UserName Form Token
     *
     * @param token
     * @return
     */
    public String getUserEmailId(String token) {
        if (StringUtils.hasText(token) && token.startsWith(TOKEN_PREFIX)) {
            token = token.substring(7);
        }
        return Jwts.parser().setSigningKey(privateKey).parseClaimsJws(token).getBody().getSubject();
    }


    /**
     * User ID From Token
     *
     * @param token
     * @return
     */
    public String getUserId(String token) {
        if (StringUtils.hasText(token) && token.startsWith(TOKEN_PREFIX)) {
            token = token.substring(7);
        }
        return Jwts.parser().setSigningKey(privateKey).parseClaimsJws(token).getBody().getId();
    }

    /**
     * Authentication
     *
     * @param token
     * @return
     */
    public Authentication getAuthentication(String token) {
        UserDetails userDetails = userDetailService.loadUserByUsername(getUserId(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    /**
     * Validating Token
     *
     * @param token
     * @return
     */
    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(privateKey).parseClaimsJws(token);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public String resetPasswordKey(User user, Integer OTP) {
        Claims claims = Jwts.claims().setSubject(user.getEmailAddress());
        claims.put("role", user.getRole().getRoleDescription());
        claims.put("OTP", OTP);
        String token = Jwts.builder()
                .setId(user.getUserId() != null ? "" + user.getUserId() : "")
                .setClaims(claims)
                .setIssuer("ReverseGeapApi")
                .setAudience(OTP.toString())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + this.resetKeyValidity))
                .signWith(SignatureAlgorithm.HS256, privateKey)
                .compact();
        return token;
    }

    public String validateResetKey(User user, String OTP) {
        String token = user.getPasswordResetKey();
        if (token == null)
            return messageProperties.getResetPasswordLinkGenerate();
        if (isTokenExpired(token))
            return messageProperties.getResetPasswordLinkExpired();
        else if (!getOTP(token).equals(OTP))
            return messageProperties.getResetPasswordInvalidOTP();
        else
            return "success";
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    public Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(privateKey)
                .parseClaimsJws(token)
                .getBody();
    }

    public String getOTP(String token) {
        return Jwts.parser().setSigningKey(privateKey).parseClaimsJws(token).getBody().getAudience();
    }

    public String accountActivationToken(User user) {
        long now = (new Date()).getTime();
        Date validity = new Date(now + this.accountActivationTokenValidityInMilliseconds);
        Claims claims = Jwts.claims().setSubject(user.getEmailAddress());
        claims.put("role", user.getRole().getRoleDescription());
        String token = Jwts.builder()
                .setClaims(claims)
                .setIssuer("ReverseGeapApi")
                .setAudience(user.getEmailAddress())
                .setId(user.getUserId() != null ? "" + user.getUserId() : "")
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, privateKey)
                .compact();
        return token;
    }

    public String getUserIdFromAccountActivationToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(privateKey)
                .parseClaimsJws(token)
                .getBody();
        return claims.getId();
    }

    public Boolean validateAccountActivationToken(String token) {
        if (isTokenExpired(token))
            return false;
        final String emailId = getUserEmailIdAccountActivationFromToken(token);
        User user;
        if (!emailId.equalsIgnoreCase("")) {
            user = userRepository.findByEmailAddress(emailId);
        } else {
            user = userRepository.findById(Long.parseLong(getUserIdFromAccountActivationToken(token))).get();
        }
        if (user.getAccountActivationKey().equals(token)) {
            user.setAccountActivationKey(null);
            user.setUserStatus(userStatusRepository.findById(1l).get());
            userRepository.save(user);
            return true;
        } else
            return false;
    }

    public String getUserEmailIdAccountActivationFromToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(privateKey)
                .parseClaimsJws(token)
                .getBody();
        return claims.getSubject();
    }
}
