package com.api.reversegear.domain.client;

import com.api.reversegear.config.Auditable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Entity
@Table(name = "reverse_gear_client")
@EqualsAndHashCode(callSuper = false)
public class Client extends Auditable<String> implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_gen")
    @SequenceGenerator(name = "client_gen", sequenceName = "client_id_gen", allocationSize = 1)
    private Long clientId;

    @NotNull(message = "First name should not be null")
    private String firstName;

    private String lastName;

    @Column(unique = true)
    private String emailAddress;

    @JsonIgnore
    private String token;

}
