package com.api.reversegear.domain.client;

import com.api.reversegear.config.Auditable;
import com.api.reversegear.domain.client.Client;
import com.api.reversegear.domain.user.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;


@Data
@Entity
@Table(name = "reverse_gear_client_account")
@EqualsAndHashCode(callSuper = false)
public class ClientAccount extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_account_gen")
    @SequenceGenerator(name = "client_account_gen", sequenceName = "client_account_id_gen", allocationSize = 1)
    private Long clientAccountId;


    @OneToOne
    @JoinColumn(name = "user_id", updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;

    @ManyToOne
    @JoinColumn(name = "clientId", updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Client client;
}
