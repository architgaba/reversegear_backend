package com.api.reversegear.domain.user;

import com.api.reversegear.config.Auditable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "reverse_gear_users")
@EqualsAndHashCode(callSuper = false)
public class User extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_gen")
    @SequenceGenerator(name = "user_gen", sequenceName = "user_id_gen", allocationSize = 1)
    private Long userId;

    @NotNull(message = "First name should not be null")
    private String firstName;

    private String lastName;

    @Column(length = 15)
    private String contactNumber;

    private String password;

    @Column(unique = true)
    private String emailAddress;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "role_id", nullable = false)
    @JsonIgnore
    private UserRole role;

    @JsonIgnore
    @Column(length = 1024)
    private String passwordResetKey;

    @JsonIgnore
    @Column(length = 1024)
    private String accountActivationKey;

    @JsonIgnore
    private LocalDateTime lastPasswordChangeDate;

    @JsonIgnore
    private LocalDateTime lastSignedInDate;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "status_id", nullable = false)
    @JsonIgnore
    private UserStatus userStatus;
}
