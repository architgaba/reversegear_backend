package com.api.reversegear.domain.user;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "reverse_gear_status")
@EqualsAndHashCode(callSuper = false)
public class UserStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(updatable = false, unique = true)
    private Integer statusCode;

    @NotNull
    private String statusDescription;
}
