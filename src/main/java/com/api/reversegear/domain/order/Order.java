package com.api.reversegear.domain.order;

import com.api.reversegear.config.Auditable;
import com.api.reversegear.domain.client.Client;
import lombok.Data;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
@Data
@Entity
@Table(name = "orders")
public class Order extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_gen")
    @SequenceGenerator(name = "order_gen", sequenceName = "order_id_gen", allocationSize = 1)
    @Column(name = "order_id" , nullable = false)
    private Long orderId;

     @NotNull
     @Column(name = "order_name")
    private  String order_name;

    @ManyToOne
    @JoinColumn(name = "clientId")
    private Client client;


}
