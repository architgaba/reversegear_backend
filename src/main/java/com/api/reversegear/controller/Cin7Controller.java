package com.api.reversegear.controller;

import com.api.reversegear.service.cin7.Cin7Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cin7")
@CrossOrigin
public class Cin7Controller {

    @Autowired
    private Cin7Service cin7Service;

    @GetMapping("/contacts")
    public ResponseEntity<?> contacts(@RequestParam(required = false,name = "where") String filter){
        return cin7Service.contacts(filter);
    }
}
