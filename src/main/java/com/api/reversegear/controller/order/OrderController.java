package com.api.reversegear.controller.order;


import com.api.reversegear.domain.order.Order;
import com.api.reversegear.repository.order.OrderRepository;
import com.api.reversegear.service.order.OrderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/order")
public class OrderController {


    private static final Logger logger = LogManager.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;


        @PostMapping("/create")
        public ResponseEntity<?> createOrder(@RequestBody Order order,  @RequestHeader("Authorization") String authKey)
        {

            logger.info("method ::: createOrder");
            return orderService.createOrder(order, authKey);

        }

        @GetMapping("/get-all")
        public ResponseEntity<?> getAllOrders(@RequestHeader("Authorization") String authKey) {
            logger.info("method ::: getAll");
            return orderService.getAllOrders();
        }

        @PutMapping("/updateorder")
        public ResponseEntity<?> updateOrder(@RequestBody Order order,  @RequestHeader("Authorization") String authKey)
        {
            logger.info("method ::: updateorder");
            return orderService.updateOrder(order, authKey);

        }

    @PutMapping("/updateorder")
    public ResponseEntity<?> deleteOrder(@RequestBody Order order,  @RequestHeader("Authorization") String authKey)
          {
            logger.info("method ::: updateorder");
            return orderService.deleteOrder(order, authKey);

          }



          }

