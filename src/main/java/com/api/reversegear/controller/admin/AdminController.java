package com.api.reversegear.controller.admin;

import com.api.reversegear.domain.client.Client;
import com.api.reversegear.domain.user.User;
import com.api.reversegear.service.client.ClientService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/admin")
@CrossOrigin
public class AdminController {

    private static final Logger logger = LogManager.getLogger(AdminController.class);

    @Autowired
    private ClientService clientService;

    @PostMapping("/create/client")
    public ResponseEntity<?> createClient(@RequestHeader("Authorization") String authKey, @RequestBody Client client, HttpServletRequest request) {
        logger.info("method ::: createSupplier");
        String url = (request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath());
        return clientService.createClient(client, url);
    }

    @GetMapping("/suppliers")
    public ResponseEntity<?> getAllSuppliers(@RequestHeader("Authorization") String authKey){
        logger.info("method ::: getAllSuppliers");
        return clientService.getAllSuppliers();
    }
}
