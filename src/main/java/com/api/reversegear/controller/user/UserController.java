package com.api.reversegear.controller.user;

import com.api.reversegear.domain.user.User;
import com.api.reversegear.dto.user.Authentication;
import com.api.reversegear.dto.user.ResetPassword;
import com.api.reversegear.dto.user.UserActivationDTO;
import com.api.reversegear.service.user.UserService;
import com.api.reversegear.utils.response.AppProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    private static final Logger logger = LogManager.getLogger(UserController.class);

    @Autowired
    private UserService userService;
    @Autowired
    private AppProperties appProperties;

    /**
     * User Authentication/Login
     *
     * @param authentication
     * @return authorization token
     */
    @PostMapping("/authenticate")
    public ResponseEntity<?> userAuthentication(@RequestBody Authentication authentication) {
        logger.info("method ::: userAuthentication");
        return userService.userAuthentication(authentication);
    }

    @PostMapping("/register")
    public ResponseEntity<?> userRegistration(@RequestBody User user, HttpServletRequest request) {
        logger.info("method ::: userRegistration");
        String url = (request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath());
        return userService.userRegistration(user, url);
    }

    @GetMapping("/forgot-password/{emailAddress}")
    public ResponseEntity<?> forgotPassword(@PathVariable String emailAddress) {
        logger.info("method ::: forgotPassword");
        return userService.forgotPassword(emailAddress);
    }

    @PutMapping("/reset-password")
    public ResponseEntity<?> resetPassword(@RequestBody ResetPassword resetPassword) {
        logger.info("method ::: resetPassword");
        return userService.resetPassword(resetPassword);
    }

    @PutMapping("/activate/account/{id}")
    public ResponseEntity<?> customerAccountActivate(@PathVariable("id") String activationKey, @RequestBody String password) {
        logger.info("method ::: customerAccountActivate");
        return userService.customerAccountActivate(activationKey, password);
    }

    @GetMapping("/activate/{id}")
    public RedirectView activateUser(@PathVariable("id") String id) {
        logger.info("method ::: activateUser");
        try {
            if (userService.redirectView(id))
                return new RedirectView(appProperties.getUserRedirect() + id);
            else
                return new RedirectView(appProperties.getLoginPageRedirectError());
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            return new RedirectView(appProperties.getLoginPageRedirectError());
        }
    }


    @GetMapping("/activate/account/{id}")
    public RedirectView redirectView(@PathVariable("id") String id) {
        logger.info("method ::: redirectView");
        try {
            if (userService.redirectView(id))
                return new RedirectView(appProperties.getLoginPageRedirect());
            else
                return new RedirectView(appProperties.getLoginPageRedirectError());
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            return new RedirectView(appProperties.getLoginPageRedirectError());
        }
    }


    @GetMapping("/admin/activate/{id}")
    public RedirectView activateSupplier(@PathVariable("id") String id) {
        logger.info("method ::: redirectView");
        try {
            if (userService.redirectView(id))
                return new RedirectView(appProperties.getUserRedirect() + id);
            else
                return new RedirectView(appProperties.getLoginPageRedirectError());
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            return new RedirectView(appProperties.getLoginPageRedirectError());
        }
    }

    @GetMapping("/authorize")
    ResponseEntity<?> authorize(@RequestHeader("Authorization") String authKey) {
        logger.info("method ::: authorize");
        return userService.authorize(authKey);
    }

    @GetMapping("/information/personal")
    public ResponseEntity<?> getUserInformation(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: getUserInformation");
        return userService.getUserInformation(auth);
    }

    @PutMapping("/update/password")
    public ResponseEntity<?> updatePassword(@RequestHeader("Authorization") String auth, @RequestBody UserActivationDTO userActivationDTO) {
        logger.info("method ::: updatePassword");
        return userService.updatePassword(auth, userActivationDTO);
    }

    @PutMapping("/update/profile")
    public ResponseEntity<?> updateProfile(@RequestHeader("Authorization") String auth, @RequestBody User user) {
        logger.info("method ::: updateProfile");
        return userService.updateProfile(auth, user);
    }
}
