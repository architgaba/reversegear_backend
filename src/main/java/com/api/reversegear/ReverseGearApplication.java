package com.api.reversegear;

import com.api.reversegear.domain.user.User;
import com.api.reversegear.repository.user.RoleRepository;
import com.api.reversegear.repository.user.UserRepository;
import com.api.reversegear.repository.user.UserStatusRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class ReverseGearApplication implements CommandLineRunner {


    private static final Logger logger = LogManager.getLogger(ReverseGearApplication.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserStatusRepository userStatusRepository;

    public static void main(String[] args) {
        SpringApplication.run(ReverseGearApplication.class, args);

        logger.info("|------------------------------------------|");
        logger.info("|                                          |");
        logger.info("|          REVERSE GEAR SERVER START       |");
        logger.info("|                                          |");
        logger.info("|------------------------------------------|");

    }

    @Override
    public void run(String... args) throws Exception {
        User super_admin = new User();
        super_admin.setFirstName("nayab");
        super_admin.setLastName("haider");
        super_admin.setPassword(passwordEncoder.encode("123"));
        super_admin.setEmailAddress("nayab.haider@niletechnologies.com");
        super_admin.setRole(roleRepository.findByRoleCode(1));
        super_admin.setAccountActivationKey(null);
        super_admin.setUserStatus(userStatusRepository.findById(1l).get());
        userRepository.save(super_admin);
        logger.info("Super_admin created successfully");

    }
}
