insert into reverse_gear_role VALUES ('1','1','ROLE_SUPER_ADMIN') ON CONFLICT DO  NOTHING;
insert into reverse_gear_role VALUES ('2','2','ROLE_ADMIN') ON CONFLICT DO  NOTHING;
insert into reverse_gear_role VALUES ('3','3','ROLE_CUSTOMER') ON CONFLICT DO  NOTHING;
insert into reverse_gear_role VALUES ('4','4','ROLE_CLIENT') ON CONFLICT DO  NOTHING;
insert into reverse_gear_role VALUES ('5','5','ROLE_GUEST') ON CONFLICT DO  NOTHING;


insert into reverse_gear_status VALUES ('1','1','ACTIVE') ON CONFLICT DO  NOTHING;
insert into reverse_gear_status VALUES ('2','2','INACTIVE') ON CONFLICT DO  NOTHING;
insert into reverse_gear_status VALUES ('3','3','ACTIVATION_PENDINIG') ON CONFLICT DO  NOTHING;



--insert into reverse_gear_supplier VALUES ('1','3','ACTIVATION_PENDINIG') ON CONFLICT DO  NOTHING;

